package ru.kso.oop.time.changeEnd;

/**
 * Интерфейс, содержащий два метода для выбора окончания для слова
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("unused")
public interface IChangeEnd {
    String changeEnd(double value);

    String changeEnd(int value);
}
