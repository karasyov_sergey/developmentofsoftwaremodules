package ru.kso.sort;

import java.util.Arrays;

/**
 * Класс, содержащий метод сортировки массива {@code numbers} методом выбора и его  реализацию
 *
 * @author KSO 17ИТ17
 */
public class SortOfChoose {
    public static void main(String[] args) {
        int[] numbers = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        sortOfChoose(numbers);
        System.out.println(Arrays.toString(numbers));
    }

    /**
     * Метод для сортировки массива {@code array} способом выбора
     *
     * @param array массив, содержащий целые числа
     */
    private static void sortOfChoose(int[] array) {
        int indexOfMin;
        for (int out = 0; out < array.length; out++) {
            indexOfMin = out;
            for (int i = out + 1; i < array.length; i++) {
                if (array[indexOfMin] > array[i]) {
                    indexOfMin = i;
                }
            }
            int cell = array[indexOfMin];
            array[indexOfMin] = array[out];
            array[out] = cell;


        }
    }
}
