package ru.kso.oop.replacer.printers;

/**
 * Класс, имплеминтирующий интерфейс iPrinter
 *
 * @author KSO 17ИТ17
 */
public class ConsolePrinter implements IPrinter {
    /**
     * Консольный принтер
     *
     * @param text текст
     */
    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
