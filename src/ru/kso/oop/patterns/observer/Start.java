package ru.kso.oop.patterns.observer;


import ru.kso.oop.patterns.observer.publisher.Publisher;
import ru.kso.oop.patterns.observer.subscriber.Subscriber1;
import ru.kso.oop.patterns.observer.subscriber.Subscriber2;

/**
 * Демонстрация классов, содержащиеся в пакетах publisher  subscriber
 *
 * @author KSO 17ИТ17
 */
public class Start {
    public static void main(String[] args) {
        Subscriber1 subscriber1 = new Subscriber1();
        Subscriber2 subscriber2 = new Subscriber2();

        Publisher publisher = new Publisher();
        publisher.addListener(subscriber1);
        publisher.addListener(subscriber2);

        publisher.createNewMessage("Message!");
        publisher.removeListener(subscriber1);
        publisher.createNewMessage("Message Two");
        publisher.removeAllListeners();
        System.out.println(publisher.getListeners());

    }
}
