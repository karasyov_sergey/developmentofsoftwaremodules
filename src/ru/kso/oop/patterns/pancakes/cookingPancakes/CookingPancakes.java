package ru.kso.oop.patterns.pancakes.cookingPancakes;


import ru.kso.oop.patterns.pancakes.elementsOfCooking.Dishes;
import ru.kso.oop.patterns.pancakes.elementsOfCooking.Dough;
import ru.kso.oop.patterns.pancakes.elementsOfCooking.Pancake;

/**
 * Класс для приготовления блинов
 *
 * @author KSO 17ИТ17
 */
public class CookingPancakes {
    private Dough dough = new Dough();
    private Dishes dishes = new Dishes();
    private Pancake pancake = new Pancake();

    /**
     * Приготовление блинов
     *
     * @param quantity количество требуемых блинов
     */
    public void cookingPancakes(int quantity) {
        dough.ready();
        dishes.ready();
        for (int index = 1; index <= quantity; index++) {
            pancake.ready();
        }
    }
}
