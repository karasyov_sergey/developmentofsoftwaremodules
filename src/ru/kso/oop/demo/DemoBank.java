package ru.kso.oop.demo;

import ru.kso.MyScanner;
import ru.kso.oop.accountofbank.AccountOfBank;

/**
 * Демонстрация работы класса AccountOfBank
 *
 * @author KSO 17ИТ17
 */
public class DemoBank {
    public static void main(String[] args) {
        AccountOfBank vtbAcc = new AccountOfBank("26987333", "Robert");
        AccountOfBank sbAcc = new AccountOfBank("6876496743", "Robert");
        AccountOfBank.Card vtbCard = vtbAcc.new Card("2299 3366 7700 5555");
        AccountOfBank.Card cardWorld = sbAcc.new Card("2288 9900 6677 3333");
        AccountOfBank.Card mastercard = sbAcc.new Card("2222 3333 4444 5555");

        AccountOfBank[] accounts = {vtbAcc, sbAcc};
        AccountOfBank.Card[] cards = {vtbCard, cardWorld, mastercard};
        changeCard(accounts, cards);


    }

    /**
     * Метод, организующий выбор банковской карты и аккаунта для дальнейшего взаимодействия с ними
     *
     * @param accounts массив, содержащий банковские аккаунты
     * @param cards    массив, содержащий банковские карты
     */
    private static void changeCard(AccountOfBank[] accounts, AccountOfBank.Card[] cards) {
        System.out.println("Вставьте карту: ");
        String card = MyScanner.scanner.next();
        if (card.equalsIgnoreCase("втб")) {
            dealWithTheUser(accounts[0], cards[0]);
        } else if (card.equalsIgnoreCase("мир")) {
            dealWithTheUser(accounts[1], cards[1]);
        } else if (card.equalsIgnoreCase("мастеркард")) {
            dealWithTheUser(accounts[1], cards[2]);
        }
        System.out.println("Хотите вставить другую карту? (Y/N): ");
        String action = MyScanner.scanner.next();
        if (action.equalsIgnoreCase("Y")) {
            changeCard(accounts, cards);
        } else if (action.equalsIgnoreCase("N")) {
            System.out.println("Обслуживание завершено");
        }

    }

    /**
     * Метод, организующий взаимодействие с пользователем
     *
     * @param accountOfBank банковский аккаунт
     * @param card          банковская карта
     */
    private static void dealWithTheUser(AccountOfBank accountOfBank, AccountOfBank.Card card) {
        System.out.println("Выберите действие " + "\n" +
                "примечание: " + "\n" +
                "1 - узнать баланс;" + "\n" +
                "2 - снять деньги;" + "\n" +
                "3 - пополнить счет" + "\n" +
                "4 - вывести имя пользователя" + "\n" +
                "5 - вывести номер аккаунта" + "\n" +
                "6 - вывести номер карты");
        String action = MyScanner.scanner.next();
        doAction(accountOfBank, card, action);
        System.out.print("Продолжить обслуживание? (Y/N): ");
        action = MyScanner.scanner.next();
        if (action.equalsIgnoreCase("Y")) {
            dealWithTheUser(accountOfBank, card);

        } else if (action.equals("N")) {
            System.out.println("Обслуживание завершено");
        }
    }

    /**
     * Метод для выполнения действия над банковским аккаунтом и картой, в зависимости от выбора пользователя
     *
     * @param accountOfBank банковский аккаунт
     * @param card          банковская карты
     * @param action        номер действия
     */
    private static void doAction(AccountOfBank accountOfBank, AccountOfBank.Card card, String action) {
        switch (action) {
            case "1":
                System.out.println(accountOfBank.getAmount());
                break;

            case "2":
                System.out.print("Введите сумму для снятия: ");
                long amountToWithdraw = MyScanner.scanner.nextLong();
                System.out.println(card.withdraw(amountToWithdraw));
                break;

            case "3":
                System.out.println("Введите сумму");
                long amountToDeposit = MyScanner.scanner.nextLong();
                System.out.println(card.deposit(amountToDeposit));
                break;

            case "4":
                System.out.println(accountOfBank.getUser());
                break;

            case "5":
                System.out.println(accountOfBank.getNumber());
                break;

            case "6":
                System.out.println(card.getNumber());
                break;
        }
    }
}

