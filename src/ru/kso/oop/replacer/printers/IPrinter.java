package ru.kso.oop.replacer.printers;

/**
 * Интерфейс, содержащий один метод
 *
 * @author KSO 17ИТ17
 */
public interface IPrinter {

    /**
     * Метод для вывода текста на экран
     *
     * @param text текст
     */
    void print(final String text);
}
