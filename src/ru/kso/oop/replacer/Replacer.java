package ru.kso.oop.replacer;

import ru.kso.oop.replacer.printers.IPrinter;
import ru.kso.oop.replacer.readers.IReader;

/**
 * Класс, инициализирующий реплейсер для смайлов
 *
 * @author KSO 17ИТ17
 */
class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    /**
     * Метод для изменения смайлов
     */
    void replace() {
        final String text = reader.read();
        final String replacedText = text.replaceAll(":\\)", ":-)");
        printer.print(replacedText);
    }
}
