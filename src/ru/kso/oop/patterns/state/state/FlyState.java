package ru.kso.oop.patterns.state.state;

/**
 * Состояние полета
 *
 * @author KSO 17ИТ17
 */
public class FlyState implements TransformerState {

    /**
     * Действие выполняемое обькт-состоянием
     */
    @Override
    public void action() {
        System.out.println("Летает");
    }
}
