package ru.kso.sequentialSearch;

/**
 * Класс, определяющий индекс заданного числа в массиве
 *
 * @author KSO 17ИТ17
 */
public class SequentialSearch {
    public static void main(String[] args) {
        int[] arrayOfNumbers = new int[(int) (100 + Math.random() * 200)];
        for (int index = 0; index < arrayOfNumbers.length; index++) {
            arrayOfNumbers[index] = (int) (-100 + Math.random() * 200);
        }
        System.out.println(indexOfNumber(arrayOfNumbers, 13));
        System.out.println(indexOfNumber(arrayOfNumbers, 22));
    }

    /**
     * Метод для последовательного поиска инлекса числа {@code number}  в ммассиве
     *
     * @param arrayOfNumbers массив, содержащий рандомные числа
     * @param number определяемое число
     * @return индекс определяемого числа
     */
    private static int indexOfNumber(int[] arrayOfNumbers, int number) {
        for (int index = 0; index < arrayOfNumbers.length; index++) {
            if (arrayOfNumbers[index] == number) {
                return index;
            }
        }
        return -1;
    }
}
