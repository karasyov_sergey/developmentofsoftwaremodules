package ru.kso.binarySearchInArrayList;

import ru.kso.MyScanner;
import ru.kso.sequentialSearchInArrayList.SequentialSearchInArrayList;

import java.util.ArrayList;

/**
 * Класс, предназначенный для бинарного поиска числа {@code number} в списочном массиве {@code integerNumbers}
 * содержащий целые числа
 *
 * @author KSO 17ИТ17
 */
public class BinarySearchInArrayList {
    private static final String ENTER_NUMBER = "Введите число: ";
    public static void main(String[] args) {
        ArrayList<Integer> integerNumbers = new ArrayList<>();
        SequentialSearchInArrayList.fill(integerNumbers);
        System.out.print(ENTER_NUMBER);
        int number = MyScanner.scanner.nextInt();
        System.out.println(binarySearchInArrayList(integerNumbers, number));
    }

    /**
     * Метод, предназначенный для бинарного поиска целого числа {@code number} в списочном массиве {@code integerNumbers}
     *
     * @param integerNumbers списочный массив, содержащий целые числа
     * @param number         целое число, вводимое пользователем с клавиатуры
     * @return индекс числа в списочном массиве {@code integerNumbers} или -1, если такое число не содежрится в массиве
     */
    private static int binarySearchInArrayList(ArrayList<Integer> integerNumbers, int number) {
        int minIndex = 0;
        int maxIndex = integerNumbers.size() - 1;
        while (minIndex <= maxIndex) {
            int midIndex = (minIndex + maxIndex) / 2;
            if (number == integerNumbers.get(midIndex)) {
                return midIndex;
            } else if (number < integerNumbers.get(midIndex)) {
                maxIndex = midIndex - 1;
            } else {
                minIndex = midIndex + 1;
            }
        }
        return -1;
    }
}
