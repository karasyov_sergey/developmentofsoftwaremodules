package ru.kso.guessTheNumber;

import ru.kso.MyScanner;

/**
 * Класс, содержащий реализацию игры "угадай число"
 *
 * @author KSO 17ИТ17
 */
public class GuessTheNumber {
    private static final String ENTER_MIN_VALUE = "Введите минимальное значение: ";
    private static final String ENTER_QUANTITY_OF_NUMBERS = "Введите количество значений: ";
    private static final int MIN = 0;
    private static final int MAX = 1;

    public static void main(String[] args) {
        System.out.print(ENTER_MIN_VALUE);
        int min = MyScanner.scanner.nextInt();
        System.out.print(ENTER_QUANTITY_OF_NUMBERS);
        int quantityOfNumbers = MyScanner.scanner.nextInt();
        int[] range = {min, quantityOfNumbers + min};
        int unknownNumber = (int) (min + Math.random() * quantityOfNumbers);
        guessTheNumber(unknownNumber, range);
    }

    /**
     * Метод для определения неизвестного числа при помощи ввода с клавиатуры
     *
     * @param unknownNumber число, загаданное компьютером
     * @param range         массив, содержащий минимальное и максимальное значения
     */
    private static void guessTheNumber(int unknownNumber, int[] range) {
        int quantityOfAttempts = getAttempts(range[MAX]);
        System.out.print("Введите число в заданном диапазоне: ");
        int number = MyScanner.scanner.nextInt();
        quantityOfAttempts--;
        do {
            if (isMore(number, unknownNumber, range)) {
                quantityOfAttempts--;
                System.out.println("Число больше загаданного");
                number = MyScanner.scanner.nextInt();
            } else if (isLess(number, unknownNumber, range)) {
                quantityOfAttempts--;
                System.out.println("Число меньше загаданного");
                number = MyScanner.scanner.nextInt();
            } else if (isBeyondTheScope(number, range)) {
                quantityOfAttempts--;
                System.out.println("Вы вышли за пределы диапазона");
                number = MyScanner.scanner.nextInt();
            }
            if (quantityOfAttempts == 0) {
                System.out.println("Вы исчерпали все попытки");
                System.exit(0);
            }
        } while (number != unknownNumber);
        System.out.println("Правильно");
    }

    /**
     * Метод для получения попыток
     *
     * @param max максимальное значение
     * @return количество попыток в зависимости от максимального значения {@code max}
     */
    private static int getAttempts(int max) {
        if (max <= 100) {
            return 7;
        } else if (max <= 1000) {
            return 25;
        }
        return 50;
    }

    /**
     * Метод, опредялющий вышло ли значение {@code number} за рамки
     *
     * @param number число
     * @param range диапазон
     * @return true if вышло за рамки else false
     */
    private static boolean isBeyondTheScope(int number, int[] range) {
        return number > range[MAX] || number < range[MIN];
    }

    /**
     * Метод, определяющий меньше ли значение {@code number} загаданного компьютером числа {@code unknownNumber}
     *
     * @param number число
     * @param unknownNumber загаданного компьютером число
     * @param range диапазон
     * @return true if число меньше загаданного компьютером  else false
     */
    private static boolean isLess(int number, int unknownNumber, int[] range) {
        return number < unknownNumber && number >= range[MIN] && number <= range[MAX];
    }

    /**
     * Метод, определяющий больше ли значение {@code number} загаданного компьютером числа {@code unknownNumber}
     *
     * @param number число
     * @param unknownNumber загаданного компьютером число
     * @param range диапазон
     * @return true if число больше загаданного компьтером else false
     */
    private static boolean isMore(int number, int unknownNumber, int[] range) {
        return number > unknownNumber && number >= range[MIN] && number <= range[MAX];
    }
}
