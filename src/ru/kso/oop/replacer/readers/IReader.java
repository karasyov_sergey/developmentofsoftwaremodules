package ru.kso.oop.replacer.readers;

/**
 * Интерфейс, содержащий один метод для чтения строки
 *
 * @author KSo 17ИТ17
 */
public interface IReader {
    /**
     * Метод для чтения строки
     *
     * @return строка
     */
    String read();
}
