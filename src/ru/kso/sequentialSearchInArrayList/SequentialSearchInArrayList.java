package ru.kso.sequentialSearchInArrayList;

import ru.kso.MyScanner;

import java.util.ArrayList;

/**
 * Класс, предназначенный для последовательного поиска числа {@code number}, вводимого с клавиатуры
 *
 * @author KSO 17ИТ17
 */
public class SequentialSearchInArrayList {
    private static final String ENTER_NUMBER = "Введите число, которое желаете найти: ";

    public static void main(String[] args) {
        ArrayList<Integer> integerNumbers = new ArrayList<>();
        fill(integerNumbers);
        System.out.println(integerNumbers);
        System.out.print(ENTER_NUMBER);
        int number = MyScanner.scanner.nextInt();
        System.out.println(sequentialSearchInArrayList(integerNumbers, number));
    }

    /**
     * Метод, последовательно ищущий целое число в списочном массиве {@code integerNumbers}
     *
     * @param integerNumbers списочный массив, содержащий целые числа
     * @param number         целое число, вводимое пользователем с клавиатуры
     * @return индекс числа в списочном массиве или -1, если число не обнаружено
     */
    private static int sequentialSearchInArrayList(ArrayList<Integer> integerNumbers, int number) {
        for (int index = 0; index < integerNumbers.size(); index++) {
            if (integerNumbers.get(index) == number) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Метод для рандомного заполнения списочного массива {@code integerNumbers} целыми числами от -10 до 10
     *
     * @param integerNumbers списочный массив,
     */
    public static void fill(ArrayList<Integer> integerNumbers) {
        for (int index = 0; index < (int) (10 + Math.random() * 90); index++) {
            integerNumbers.add(index, (int) (-10 + Math.random() * 20));
        }
    }
}
