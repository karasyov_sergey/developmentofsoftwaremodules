package ru.kso.fibonacci;

import ru.kso.MyScanner;


/**
 * Класс, содержащий метод вычисления числа фибоначчи и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class NumberOfFibonacci {
    public static void main(String[] args) {
        System.out.print("Ввеедите индекс числа Фибоначчи: ");
        int indexOfNumber = MyScanner.scanner.nextInt();
        System.out.println(fibonacciNumber(indexOfNumber));

    }

    /**
     * Метод, возвращающий число Фибоначчи по индексу
     *
     * @param indexOfNumber индекс числа Фибоначчи
     * @return число Фибоначчи
     */
    private static long fibonacciNumber(int indexOfNumber) {
        int tempOne = 0;
        int tempTwo = 1;
        int numberOfFibonacci = 1;
        for (int i = 1; i <= indexOfNumber; i++) {
            numberOfFibonacci = tempOne + tempTwo;
            tempOne = tempTwo;
            tempTwo = numberOfFibonacci;
        }
        return numberOfFibonacci;
    }
}
