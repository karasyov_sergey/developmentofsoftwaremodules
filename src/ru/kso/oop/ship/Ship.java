package ru.kso.oop.ship;

import java.util.ArrayList;

/**
 * Класс Ship, содержащий конструктор для инициализации объекта типа Ship и методы для работы с ним
 *
 * @author KSO 17ИТ17
 */
public class Ship {
    private ArrayList<Integer> coordinates = new ArrayList<>();

    public Ship(int start) {
        coordinates.add(start);
        coordinates.add(start + 1);
        coordinates.add(start + 2);
    }


    @Override
    public String toString() {
        return "Ship{" +
                "coordinates=" + coordinates +
                '}';
    }


    /**
     * Метод, определяющий уничтожен ли корабль
     *
     * @return true если уничтожен или false если не уничтожен
     */
    public boolean isDestroyed() {
        return coordinates.isEmpty();
    }

    /**
     * Метод, определяющий попал ли выстрел в корабль
     *
     * @param shot выстрел
     * @return true если попал или false если не попал
     */
    public boolean isHit(int shot) {
        for (Integer coordinate : coordinates) {
            if (coordinate == shot) {
                coordinates.remove(coordinate);
                return true;
            }
        }
        return false;
    }

    /**
     * Метод для определения повторного выстрела
     *
     * @param shots список выстрелов
     * @param shot выстрел
     * @return true если выстрел был сделан ранее иначе false
     */
    public static boolean isRepeatedShot(ArrayList<Integer> shots, int shot) {
        for (Integer aShot : shots) {
            if (aShot == shot) {
                return true;
            }
        }
        return false;
    }

}
