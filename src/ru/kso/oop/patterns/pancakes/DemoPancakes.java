package ru.kso.oop.patterns.pancakes;



import ru.kso.oop.patterns.pancakes.cookingPancakes.CookingPancakes;

import java.util.Scanner;

/**
 * Демонстрация пакета pancakes
 *
 * @author KSO 17ИТ17
 */
public class DemoPancakes {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        CookingPancakes cookingPancakes = new CookingPancakes();
        System.out.print("Введите количество порций: ");
        int quantity = scanner.nextInt();
        cookingPancakes.cookingPancakes(quantity);
    }
}
