package ru.kso.oop.patterns.state.state;

/**
 * Комбинированное состояние
 *
 * @author KSO 17ИТ17
 */
public class ComboState implements TransformerState {

    /**
     * Действие выполняемое обькт-состоянием
     */
    @Override
    public void action() {
        new FireState().action();
        new FlyState().action();
    }
}
