package ru.kso.oop.time.cases.genitive;

import ru.kso.oop.time.cases.ICase;

/**
 * Класс, содержащий метод для возвращения окончания для слова
 * первого склонения, множественного числа, родительского падежа
 *
 * @author KSO 17ИТ17
 */
public class FirstMultiGen implements ICase {
    @Override
    public String caseOfWord() {
        return "";
    }
}
