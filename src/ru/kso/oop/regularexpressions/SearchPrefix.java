package ru.kso.oop.regularexpressions;

import java.io.*;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс предназначенный для поиска слов с файла, начинающееся на приставку пре или при и запись их на файл
 *
 * @author KSO 17ИТ17
 */
public class SearchPrefix {
    public static void main(String[] args) throws IOException {
        String s;
        Pattern pattern = Pattern.compile("(при).*|(пре).*|(При).*|(Пре).*");
        Matcher matcher = pattern.matcher("");
        ArrayList<String> list = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/ru/kso/oop/regularexpressions/input.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    System.out.println(matcher.group());
                    list.add(matcher.group());
                }
            }
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src/ru/kso/oop/regularexpressions/output.txt"))) {
            for (String i : list) {
                bufferedWriter.write(i + "\n");
            }
        }
    }
}
