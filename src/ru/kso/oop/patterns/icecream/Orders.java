package ru.kso.oop.patterns.icecream;


import ru.kso.oop.patterns.icecream.product.Cream;
import ru.kso.oop.patterns.icecream.product.FruitIce;
import ru.kso.oop.patterns.icecream.product.IceCream;
import ru.kso.oop.patterns.icecream.product.Popsicle;
import ru.kso.oop.patterns.icecream.decorate.*;

/**
 * Демонстрация классов, содержащихся в пакетах decorate и product
 *
 * @author KSO 17ИТ17
 */
public class Orders {
    public static void main(String[] args) {
        IceCream cream = new SyrupDecorator(new Cream());
        IceCream fruitIce = new PowderDecorator(new FruitIce());
        IceCream popsicle = new SyrupDecorator(new Popsicle());
        cream.cook();
        fruitIce.cook();
        popsicle.cook();
    }

}
