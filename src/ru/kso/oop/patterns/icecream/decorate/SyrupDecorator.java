package ru.kso.oop.patterns.icecream.decorate;


import ru.kso.oop.patterns.icecream.product.IceCream;

/**
 * Класс, расширяющий класс Decorator
 *
 * @author KSO 17ИТ17
 */
public class SyrupDecorator extends Decorator {

    public SyrupDecorator(IceCream iceCream) {
        super(iceCream);
    }

    /**
     * Метод для добавления присыпки
     */
    @Override
    void afterCook() {
        System.out.println("Сироп добавлен");
    }
}
