package ru.kso.oop.patterns.pancakes.elementsOfCooking;


import ru.kso.oop.patterns.pancakes.interfaces.IReady;

/**
 * Класс блин
 *
 * @author KSO 17ИТ17
 */
public class Pancake implements IReady {
    /**
     * Создание блина
     */
    @Override
    public void ready() {
        System.out.println("Блин готов! Ура:)))");
    }
}
