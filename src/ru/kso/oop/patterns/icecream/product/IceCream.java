package ru.kso.oop.patterns.icecream.product;

/**
 * Интерфейс, содержащий один методъ
 *
 * @author KSO 17ИТ17
 */
public interface IceCream {

    /**
     * Метод для приготовления мороженого
     */
    void cook();
}
