package ru.kso.oop.teacher;

import ru.kso.oop.person.Person;
import ru.kso.MyScanner;
/**
 * Класс Teacher, расширяющий класс Person
 *
 * @autor KSO 17ИТ17
 */
@SuppressWarnings("unused")
public class Teacher extends Person {
    private int workExperience;

    public Teacher(String surname, int yearOfBirth, int workExperience) {
        super(surname, yearOfBirth);
        this.workExperience = workExperience;
    }

    public Teacher() {
        super("Неизвестно", 0);
        workExperience = 0;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "surname='" + getSurname() + '\'' +
                ", yearOfBirth=" + getYearOfBirth() +
                ", workExperience=" + workExperience +
                '}';
    }

    /**
     * Метод для заполнения учителя
     */
    public void fillTeacher() {
        fillPerson();
        System.out.print("Ввеедите стаж работы: ");
        workExperience = MyScanner.scanner.nextInt();

    }
}
