package ru.kso.oop.accountofbank;

/**
 * Класс Account Of Bank, содержащий вложеный класс Card и методы для работы с объектами данных типов
 *
 * @author KSO 17ИТ17
 */
public class AccountOfBank {
    private final String number;
    private final String user;
    private long amount;

    public AccountOfBank(final String number, final String user) {
        this.number = number;
        this.user = user;
    }

    public String getNumber() {
        return number;
    }

    public String getUser() {
        return user;
    }

    public long getAmount() {
        return amount;
    }

    /**
     * Метод для снятия со счета
     *
     * @param amountToWithdraw сумма для снятия
     * @return снятая сумма
     */
    private long withdraw(long amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final long amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    /**
     * Метод для пополнения счета
     *
     * @param amountToDeposit сумма для пополнения
     * @return пополненный баланс
     */
    private long deposit(long amountToDeposit) {
        return amount += amountToDeposit;
    }

    /**
     * Вложеный класс Card
     */
    public class Card {
        private final String number;

        public Card(final String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }

        /**
         * Метод для снятия со счета
         *
         * @param amountToWithdraw сумма для снятия
         * @return снятая сумма
         */
        public long withdraw(final long amountToWithdraw) {
            return AccountOfBank.this.withdraw(amountToWithdraw);
        }

        /**
         * Метод для пополнения счета
         *
         * @param amountToDeposit сумма для пополнения
         * @return пополненный баланс
         */
        public long deposit(final long amountToDeposit) {
            return AccountOfBank.this.deposit(amountToDeposit);
        }
    }

}
