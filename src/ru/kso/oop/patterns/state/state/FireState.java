package ru.kso.oop.patterns.state.state;

/**
 * Состояние стрельба
 *
 * @author KSO 17ИТ17
 */
public class FireState implements TransformerState {

    /**
     * Действие выполняемое обькт-состоянием
     */
    @Override
    public void action() {
        System.out.println("Стреляет");
    }
}
