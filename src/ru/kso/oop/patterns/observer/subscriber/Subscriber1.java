package ru.kso.oop.patterns.observer.subscriber;


import ru.kso.oop.patterns.observer.publisher.PublisherActionListener;

/**
 * Инициализация подписчика 1
 *
 * @author KSO 17ИТ17
 */
public class Subscriber1 implements PublisherActionListener {
    /**
     * Метод для получения сообщения
     *
     * @param message сообщение
     */
    @Override
    public void doAction(String message) {
        System.out.println(message + " from " + this.getClass().getName());
    }

}

