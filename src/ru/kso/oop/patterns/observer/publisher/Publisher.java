package ru.kso.oop.patterns.observer.publisher;


import java.util.ArrayList;

/**
 * Класс, имплементирующий интерфейс IPublisher
 *
 * @author KSO 17ИТ17
 */
public class Publisher implements IPublisher {
    private ArrayList<PublisherActionListener> listeners = new ArrayList<>();

    /**
     * Метод для получения списка слушателей
     *
     * @return список слушателей
     */
    @Override
    public ArrayList<PublisherActionListener> getListeners() {
        return listeners;
    }

    /**
     * Метод для добавления слушателя
     *
     * @param listener слушатель
     */
    @Override
    public void addListener(PublisherActionListener listener) {
        listeners.add(listener);
    }

    /**
     * Метод для удаления слушателя
     *
     * @param listener слушатель
     */
    @Override
    public void removeListener(PublisherActionListener listener) {
        listeners.remove(listener);
    }

    /**
     * Метод для удаления всех слушателей
     */
    @Override
    public void removeAllListeners() {
        listeners.clear();
    }

    /**
     * Метод для оповещения слушателей
     *
     * @param message сообщение
     */
    @Override
    public void notifySubscribers(String message) {
        for (PublisherActionListener actionListener : listeners) {
            actionListener.doAction(message);
        }
    }

    /**
     * Метод для создания и рассылки сообщения
     *
     * @param message сообщение
     */
    public void createNewMessage(String message) {
        System.out.println("Publisher printed message " + message);
        notifySubscribers(message);
    }


}


