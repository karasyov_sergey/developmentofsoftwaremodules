package ru.kso.sort;

import java.util.ArrayList;

/**
 * Класс, содержащий метод сортировки списочного массива {@code numbers} путем выбора и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class SortOfChooseArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        fill(numbers);
        inverseSortOfChoose(numbers);
        System.out.println(numbers);
    }

    /**
     *Метод для сортировки по убыванию списочного массива {@code arrayList} путем выбора
     *
     * @param arrayList списочный массив, содержащий целые числа
     */
    private static void inverseSortOfChoose(ArrayList<Integer> arrayList) {
        int minIndex;
        for (int out = 0; out < arrayList.size(); out++) {
            minIndex = out;
            for (int index = out + 1; index < arrayList.size(); index++) {
                if (arrayList.get(minIndex) < arrayList.get(index)) {
                    minIndex = index;
                }
            }
            int temp = arrayList.get(minIndex);
            arrayList.set(minIndex, arrayList.get(out));
            arrayList.set(out, temp);
        }
    }

    /**
     *Метод, задающий рандомно размер списочного массива {@code arrayList} и заполняющий его элементы от  0 до n с шагом в  1
     *
     * @param arrayList списочный массив
     */
    private static void fill(ArrayList<Integer> arrayList) {
        for (int index = 0; index < ((int) (5 + Math.random() * 10)); index++) {
            arrayList.add(index);
        }
    }
}
