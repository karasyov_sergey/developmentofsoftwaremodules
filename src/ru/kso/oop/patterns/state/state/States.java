package ru.kso.oop.patterns.state.state;

/**
 * Перечисление состояний объекта
 *
 * @author KSO 17ИТ17
 */
public enum States {
    MOVE,
    FIRE,
    COMBO,
    FLY
}
