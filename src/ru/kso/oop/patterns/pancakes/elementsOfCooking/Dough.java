package ru.kso.oop.patterns.pancakes.elementsOfCooking;


import ru.kso.oop.patterns.pancakes.interfaces.IReady;

/**
 * Класс тесто
 *
 * @author KSO 17ИТ17
 */
public class Dough implements IReady {

    /**
     * Подготовка теста
     */
    @Override
    public void ready() {
        System.out.println("Тесто готово!");
    }
}
