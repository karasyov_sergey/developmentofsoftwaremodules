package ru.kso.oop.patterns.icecream.product;

/**
 * Класс, имплеминтирующий интерфейс IceCream
 *
 * @author KSO 17ИТ17
 */
public class Cream implements IceCream {

    /**
     * Метод для приготовления пломбира
     */
    @Override
    public void cook() {
        System.out.println("Пломбир готов");
    }
}
