package ru.kso.sort;

import java.util.Arrays;

/**
 * Класс, содержащий метод для сортировки массива {@code numbers} и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class BubblySort {
    public static void main(String[] args) {
        int[] numbers = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        bubblySort(numbers);
        System.out.println(Arrays.toString(numbers));

    }

    /**
     * Метод, сортирующий массив {@code array} по возрастанию пузырьковым способом
     *
     * @param array массив, содержащий целые числа
     */
    private static void bubblySort(int[] array) {
        for (int out = array.length - 1; out > 0; out--) {
            for (int i = 0; i < out; i++) {
                if (array[i] > array[i + 1]) {
                    int cell = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = cell;
                }
            }
        }
    }
}

