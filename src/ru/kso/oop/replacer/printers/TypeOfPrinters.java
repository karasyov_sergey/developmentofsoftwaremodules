package ru.kso.oop.replacer.printers;

/**
 * Перечисление видов принтера
 */
public enum  TypeOfPrinters {
    ADV_CONSOLE_PRINTER,
    CONSOLE_PRINTER,
    DEC_PRINTER
}
