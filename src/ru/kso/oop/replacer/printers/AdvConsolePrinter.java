package ru.kso.oop.replacer.printers;

/**
 * Класс, имплеминтирующий интерфейс IPrinter
 *
 * @author KSO 17ИТ17
 */
public class AdvConsolePrinter implements IPrinter {

    /**
     * Продвинутый консольный принтер
     *
     * @param text текст
     */
    @Override
    public void print(String text) {
        System.out.println(text + "\n" + "Длина текста = " + text.length());
    }
}
