package ru.kso.oop.patterns.pancakes.interfaces;

/**
 * Интерфейс, содержащий метод
 *
 * @author KSO 17ИТ17
 */
public interface IReady {

    void ready();
}
