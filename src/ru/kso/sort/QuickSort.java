package ru.kso.sort;

import java.util.Arrays;

/**
 * Класс, содержащий метод быстрой сортировки и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] numbers = {1, 7, 5, 4, 8, 6, 9, 2, 3};
        int leftIndex = 0;
        int rightIndex = numbers.length - 1;
        quickSort(numbers, leftIndex, rightIndex);
        System.out.println(Arrays.toString(numbers));
    }

    /**
     * Рекурсивный метод быстрой сортировки массива
     *
     * @param array      массив, содержащий целые числа
     * @param leftIndex  начальный индекс
     * @param rightIndex последний индекс
     */
    private static void quickSort(int[] array, int leftIndex, int rightIndex) {
        int i = leftIndex;
        int j = rightIndex;
        int mid = array[(rightIndex + leftIndex) / 2];
        do {
            while (array[i] < mid) {
                i++;
            }
            while (array[j] > mid) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }

        } while (i <= j);
        if (leftIndex < j) {
            quickSort(array, leftIndex, j);
        }
        if (rightIndex > i) {
            quickSort(array, i, rightIndex);
        }

    }
}
