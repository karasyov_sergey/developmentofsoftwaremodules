package ru.kso.oop.patterns.observer.publisher;

/**
 * Интерфейс, содержащий метод
 *
 * @author KSO 17ИТ17
 */
public interface PublisherActionListener {
    /**
     * Метод для выполнения действия публикатором
     *
     * @param message сообщение
     */
    void doAction(String message);
}
