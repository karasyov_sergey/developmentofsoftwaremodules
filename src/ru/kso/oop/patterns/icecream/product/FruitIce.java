package ru.kso.oop.patterns.icecream.product;

/**
 * Класс, имплеминтирующий интерфейс IceCream
 *
 * @author KSO 17ИТ17
 */
public class FruitIce implements IceCream {

    /**
     * Метод для приготовления фруктового льда
     */
    @Override
    public void cook() {
        System.out.println("Фруктовый лед готов");
    }
}
