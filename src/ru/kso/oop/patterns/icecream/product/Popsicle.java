package ru.kso.oop.patterns.icecream.product;

/**
 * Класс, имплеминтирующий интерфейс IceCream
 *
 * @author KSO 17ИТ17
 */
public class Popsicle implements IceCream {
    /**
     * Метод для приготовления эскимо
     */
    @Override
    public void cook() {
        System.out.println("Эскимо приготовлено");
    }
}
