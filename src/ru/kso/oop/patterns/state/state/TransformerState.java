package ru.kso.oop.patterns.state.state;

/**
 * Интерфейс, содержащий метод
 *
 * @author KSO 17ИТ17
 */
public interface TransformerState {
    /**
     * Действие выполняемое обькт-состоянием
     */
    void action();
}
