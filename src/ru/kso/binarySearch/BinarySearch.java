package ru.kso.binarySearch;

/**
 * Класс для поиска индекса заданного числа в массиве
 *
 * @author KSO 17ИТ17
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] arrayOfNumbers = {-15, -11, -5, 0, 7, 11, 33, 63, 79, 555};
        System.out.println(binarySearchOfIndexOfNumber(arrayOfNumbers, 33));
        System.out.println(binarySearchOfIndexOfNumber(arrayOfNumbers, -16));
    }

    /**
     * Метод для бинарного поиска индекса числа в массиве
     *
     * @param arrayOfNumbers массив с числами
     * @param number         определяемое число
     * @return индекс определяемого числа
     */
    private static int binarySearchOfIndexOfNumber(int[] arrayOfNumbers, int number) {
        int leftIndex = 0;
        int rightIndex = arrayOfNumbers.length - 1;
        while (leftIndex <= rightIndex) {
            int mid = (leftIndex + rightIndex) / 2;
            if (number == arrayOfNumbers[mid]) {
                return mid;
            } else if (number < arrayOfNumbers[mid]) {
                rightIndex = mid - 1;
            } else {
                leftIndex = mid + 1;
            }
        }
        return -1;
    }
}
