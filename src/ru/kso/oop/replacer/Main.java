package ru.kso.oop.replacer;

import ru.kso.oop.replacer.printers.IPrinter;
import ru.kso.oop.replacer.printers.PrinterSelector;
import ru.kso.oop.replacer.printers.TypeOfPrinters;
import ru.kso.oop.replacer.readers.IReader;
import ru.kso.oop.replacer.readers.PredefinedReader;

/**
 * Демонстрация классов, содержащиеся в пакетах printers, readers, и класса Replacer
 *
 * @author KSO 17ИТ17
 */
public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader("Привет:) Пока-пока:)");
        PrinterSelector selector = new PrinterSelector();
        IPrinter printer = selector.getPrinter(TypeOfPrinters.ADV_CONSOLE_PRINTER);
        Replacer replacer = new Replacer(reader,printer);
        replacer.replace();


    }
}

