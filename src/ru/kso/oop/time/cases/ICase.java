package ru.kso.oop.time.cases;

/**
 * Интерфейс, содержащий метод для возвращения окончания слова
 *
 * @author KSO 17ИТ17
 */
public interface ICase {
    String caseOfWord();
}
