package ru.kso.oop.person;

import ru.kso.MyScanner;

/**
 * Родительский класс Person
 *
 * @author KSO 17ИТ17
 */
public class Person {
    private String surname;
    private int yearOfBirth;

    public Person(String surname, int yearOfBirth) {
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public String getSurname() {
        return surname;
    }

    protected int getYearOfBirth() {
        return yearOfBirth;
    }

    protected void fillPerson(){
        System.out.print("Введите фамилию: ");
        surname = MyScanner.scanner.next();
        System.out.print("Введите год рождения: ");
        yearOfBirth = MyScanner.scanner.nextInt();
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                '}';
    }
}
