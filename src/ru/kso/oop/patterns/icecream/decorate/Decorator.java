package ru.kso.oop.patterns.icecream.decorate;


import ru.kso.oop.patterns.icecream.product.IceCream;

/**
 * Абстрактый класс для инициализации декоратора
 *
 * @author KSO 17ИТ17
 */
public abstract class Decorator implements IceCream {
    private IceCream iceCream;

    Decorator(IceCream iceCream) {
        this.iceCream = iceCream;
    }

    /**
     * Метод для дополнительного декорирования
     */
    abstract void afterCook();

    /**
     * Приготовление мороженого и дополнительное декорирование
     */
    @Override
    public void cook() {
        iceCream.cook();
        afterCook();
    }
}
