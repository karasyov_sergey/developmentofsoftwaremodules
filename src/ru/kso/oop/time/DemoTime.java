package ru.kso.oop.time;

import ru.kso.MyScanner;
import ru.kso.oop.time.changeEnd.*;

/**
 * Класс, для перевода милисекунд в секунды, минуты, часы и
 * демонстрации работы пакетов cases и changeEnd
 *
 * @author KSO 17ИТ17
 */
public class DemoTime {
    public static void main(String[] args) {
        IChangeEnd changerForMSec = new ChangerForMSec();
        IChangeEnd changerForSec = new ChangerForSec();
        IChangeEnd changerForMinute = new ChangerForMinute();
        IChangeEnd changerForHour = new ChangerForHour();
        System.out.print("Введите количество милисекунд: ");
        long mSec = MyScanner.scanner.nextInt();
        double sec = toSec(mSec);
        double minute = toMinute(mSec);
        double hour = toHour(mSec);
        System.out.printf("В %d милисекуд" + changerForMSec.changeEnd(mSec) + ":\n%.3f секунд"
                        + changerForSec.changeEnd(sec) + "\n%.3f минут" +
                        changerForMinute.changeEnd(minute) + "\n%.3f час" + changerForHour.changeEnd(hour),
                mSec, sec, minute, hour);
    }

    /**
     * Метод для перевода милисекунд в часы
     *
     * @param mSec количество милисекунд
     * @return количество часов
     */
    private static double toHour(long mSec) {
        return (double) mSec / 3600000;
    }

    /**
     * Метод для перевода милисекунд в минуты
     *
     * @param mSec количество милисекунд
     * @return количество часов
     */
    private static double toMinute(long mSec) {
        return (double) mSec / 60000;
    }

    /**
     * Метод для перевода милисекунд в секунды
     *
     * @param mSec количество милисекунд
     * @return количество секунд
     */
    private static double toSec(long mSec) {
        return (double) mSec / 1000;
    }
}
