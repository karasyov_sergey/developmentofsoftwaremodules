package ru.kso.oop.student;

import ru.kso.oop.person.Person;
import ru.kso.MyScanner;


/**
 * Класс Student, расширяющий класс Person
 *
 * @author KSo 17ИТ17
 */
@SuppressWarnings("unused")
public class Student extends Person {
    private String group;
    private int yearOfArrival;
    private String codeOfSpecialty;

    public Student(String surname, int yearOfBirth, String group, int yearOfArrival, String codeOfSpecialty) {
        super(surname, yearOfBirth);
        this.group = group;
        this.yearOfArrival = yearOfArrival;
        this.codeOfSpecialty = codeOfSpecialty;
    }

    public Student() {
        super("Неизвестно", 0);
        group = "Неизвестно";
        yearOfArrival = 0;
        codeOfSpecialty = "Неизввестно";
    }

    public String getCodeOfSpecialty() {
        return codeOfSpecialty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "surname='" + getSurname() + '\'' +
                ", yearOfBirth=" + getYearOfBirth() +
                ", group='" + group + '\'' +
                ", yearOfArrival=" + yearOfArrival +
                ", codeOfSpecialty='" + codeOfSpecialty + '\'' +
                '}';
    }

    /**
     * Метод для заполнения студента
     */
    public void fillStudent() {
        fillPerson();
        System.out.print("Ввеедите группу: ");
        group = MyScanner.scanner.next();
        System.out.print("Введите год поступления: ");
        yearOfArrival = MyScanner.scanner.nextInt();
        System.out.print("Введите код специальности студента: ");
        codeOfSpecialty = MyScanner.scanner.next();

    }
}
