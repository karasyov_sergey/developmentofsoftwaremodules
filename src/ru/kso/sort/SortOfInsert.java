package ru.kso.sort;

import java.util.Arrays;

/**
 * Класс, содержащий метод, сортирующий массив {@code numbers} путем вставки, и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class SortOfInsert {
    public static void main(String[] args) {
        int[] numbers = {2, 7, 8, 6, 5, 4, 3, 9, 1,};
        sortOfInsert(numbers);
        System.out.println(Arrays.toString(numbers));
    }

    /**
     * Метод, сортирующий массив {@code array} путем вставки
     *
     * @param array массив, содержащий целые числа
     */
    private static void sortOfInsert(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int index = out;
            int temp = array[out];
            for (int i = out - 1; i >= 0; i--) {
                if (temp < array[i]) {
                    array[i + 1] = array[i];
                    index = i;
                }
            }
            array[index] = temp;
        }
    }
}

