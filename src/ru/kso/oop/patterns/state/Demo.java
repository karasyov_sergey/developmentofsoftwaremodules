package ru.kso.oop.patterns.state;
import ru.kso.oop.patterns.state.context.TransformerContext;
import ru.kso.oop.patterns.state.state.States;

/**
 * Демонстрация пакета state
 *
 * @author KSO 17ИТ17
 */
public class Demo {
    public static void main(String[] args) {
        TransformerContext context = new TransformerContext();
        context.setState(States.COMBO);
        context.action();
        System.out.println(context.getState());
        context.setState(States.FLY);
        context.action();
        System.out.println(context.getState());
        context.setState(States.MOVE);
        context.action();
        System.out.println(context.getState());
        context.setState(States.FIRE);
        context.action();
        System.out.println(context.getState());

    }
}
