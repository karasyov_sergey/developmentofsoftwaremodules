package ru.kso.oop.time.changeEnd;

import ru.kso.oop.time.cases.prepositional.MultiPre;
import ru.kso.oop.time.cases.prepositional.SinglePre;

/**
 * Класс, содержащий методы для выбора окончания в зависимости от значения для слова "милисекунда"
 *
 * @author KSO 17ИТ17
 */
public class ChangerForMSec implements IChangeEnd {
    @Override
    public String changeEnd(double value) {
        if (value % 10 == 1 && value % 100 != 11) {
            return new SinglePre().caseOfWord();
        }
        return new MultiPre().caseOfWord();
    }


    @Override
    public String changeEnd(int value) {
        if (value % 10 == 1 && value % 100 != 11) {
            return new SinglePre().caseOfWord();
        }
        return new MultiPre().caseOfWord();
    }
}
