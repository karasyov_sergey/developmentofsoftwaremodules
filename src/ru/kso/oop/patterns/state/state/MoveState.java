package ru.kso.oop.patterns.state.state;

/**
 * Состояние передвижение
 *
 * @author KSO 17ИТ17
 */
public class MoveState implements TransformerState {

    /**
     * Действие выполняемое обькт-состоянием
     */
    @Override
    public void action() {
        System.out.println("Идет");
    }
}
