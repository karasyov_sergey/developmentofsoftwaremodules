package ru.kso.oop.replacer.readers;

/**
 * Класс, имплиментирующий интерфейс IReader
 *
 * @author KSO 17ИТ17
 */
public class PredefinedReader implements IReader {
    private String text;

    public PredefinedReader(String text) {
        this.text = text;
    }

    @Override
    public String read() {
        return text;
    }
}
