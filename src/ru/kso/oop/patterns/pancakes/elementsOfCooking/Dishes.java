package ru.kso.oop.patterns.pancakes.elementsOfCooking;


import ru.kso.oop.patterns.pancakes.interfaces.IReady;

/**
 * Класс посуда
 *
 * @author KSO 17ИТ17
 */
public class Dishes implements IReady {
    /**
     * Подготовка посуды
     */
    @Override
    public void ready() {
        System.out.println("Посуда готова!");
    }
}
