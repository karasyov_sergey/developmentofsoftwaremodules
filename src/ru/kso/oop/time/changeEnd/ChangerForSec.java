package ru.kso.oop.time.changeEnd;


import ru.kso.oop.time.cases.genitive.FirstMultiGen;
import ru.kso.oop.time.cases.genitive.FirstSingleGen;
import ru.kso.oop.time.cases.nominative.FirstNomSingle;

/**
 * Класс, содержащий методы для выбора окончания в зависимости от значения для слова "секунда"
 *
 * @author KSO 17ИТ17
 */
public class ChangerForSec implements IChangeEnd {
    @Override
    public String changeEnd(double value) {
        if (value % 10 == 2 || value % 10 == 3 || value % 10 == 4 &&
                value % 100 != 12 && value % 100 != 13 && value % 100 != 14) {
            return new FirstSingleGen().caseOfWord();
        } else {
            if (value % 10 == 1 && value % 100 != 11) {
                return new FirstNomSingle().caseOfWord();
            }
        }
        return new FirstMultiGen().caseOfWord();
    }


    @Override
    public String changeEnd(int value) {
        if (value % 10 == 2 || value % 10 == 3 || value % 10 == 4 &&
                value % 100 != 12 && value % 100 != 13 && value % 100 != 14) {
            return new FirstSingleGen().caseOfWord();
        } else {
            if (value % 10 == 1 && value % 100 != 11) {
                return new FirstNomSingle().caseOfWord();
            }
        }
        return new FirstMultiGen().caseOfWord();
    }
}
