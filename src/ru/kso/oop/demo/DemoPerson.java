package ru.kso.oop.demo;

import ru.kso.MyScanner;
import ru.kso.oop.person.Person;
import ru.kso.oop.student.Student;
import ru.kso.oop.teacher.Teacher;


import java.util.ArrayList;

/**
 * Класс, демонстрирующий работу классов Person, Student,  Teacher
 * и содержащий методы для работы со списками {@code persons, students, teachers} данного типа
 *
 * @author KSO 17ИТ17
 */
public class DemoPerson {
    public static void main(String[] args) {
        ArrayList<Person> persons = new ArrayList<>();
        ArrayList<Student> students = new ArrayList<>();
        ArrayList<Teacher> teachers = new ArrayList<>();
        fillByStudents(persons);
        fillByTeachers(persons);
        bubblySort(persons);
        changeStudents(persons, students);
        changeTeachers(persons, teachers);
        System.out.print("Введите код специальности: ");
        String specialtyCode = MyScanner.scanner.next();
        toPrintStudentsBySpecialCode(students, specialtyCode);
        toPrintListOfTeachers(teachers);
    }

    /**
     * Метод для вывода на экран учителей из списка {@code teachers}
     *
     * @param teachers список учителей
     */
    private static void toPrintListOfTeachers(ArrayList<Teacher> teachers) {
        for (Teacher teacher : teachers) {
            System.out.println(teacher);
        }
    }

    /**
     * Метод для отбора элементов типа Teacher из списка типа Person {@code persons}
     * и добавления их в список типа Teacher {@code teachers}
     *
     * @param persons  список персон
     * @param teachers список  учителей
     */
    private static void changeTeachers(ArrayList<Person> persons, ArrayList<Teacher> teachers) {
        for (Person person : persons) {
            if (person.getClass() == Teacher.class) {
                teachers.add((Teacher) person);
            }
        }
    }

    /**
     * Метод для отбора элементов типа Student из списка типа Person {@code persons}
     * и добавления их в список типа Student {@code students}
     *
     * @param persons  список персон
     * @param students список студентов
     */
    private static void changeStudents(ArrayList<Person> persons, ArrayList<Student> students) {
        for (Person person : persons) {
            if (person.getClass() == Student.class) {
                students.add((Student) person);
            }
        }
    }

    /**
     * Метод, выводящий на экран студентов по данному коду специальности {@code specialtyCode}
     *
     * @param students      список студентов
     * @param specialtyCode код специальности
     */
    private static void toPrintStudentsBySpecialCode(ArrayList<Student> students, String specialtyCode) {
        for (Student student : students) {
            if (student.getCodeOfSpecialty().equals(specialtyCode)) {
                System.out.println(student);
            }
        }
    }


    /**
     * Метод, сортирующий список персон {@code persons} по алфавиту
     *
     * @param persons список персон
     */

    private static void bubblySort(ArrayList<Person> persons) {
        for (int out = persons.size() - 1; out > 0; out--) {
            for (int i = 0; i < out; i++) {
                if (persons.get(i).getSurname().compareToIgnoreCase(persons.get(i + 1).getSurname()) > 0) {
                    Person temp = persons.get(i);
                    persons.set(i, persons.get(i + 1));
                    persons.set(i + 1, temp);
                }
            }
        }
    }

    /**
     * Метод для заполнения списка {@code persons} студентами
     *
     * @param persons список персон
     */
    private static void fillByStudents(ArrayList<Person> persons) {
        System.out.print("Введите количество студентов: ");
        int quantityOfStudents = MyScanner.scanner.nextInt();
        for (int index = 0; index < quantityOfStudents; index++) {
            Student student = new Student();
            student.fillStudent();
            persons.add(student);
        }
    }


    /**
     * Метод для заполнения списка {@code persons} учителями
     *
     * @param persons список персон
     */
    private static void fillByTeachers(ArrayList<Person> persons) {
        System.out.print("Введите количество учителей: ");
        int quantityOfTeachers = MyScanner.scanner.nextInt();
        for (int index = 0; index < quantityOfTeachers; index++) {
            Teacher teacher = new Teacher();
            teacher.fillTeacher();
            persons.add(teacher);
        }
    }


}
