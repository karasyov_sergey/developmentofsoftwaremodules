package ru.kso.oop.demo;

import ru.kso.oop.ship.Ship;
import ru.kso.MyScanner;

import java.util.ArrayList;

/**
 * Класс, демонстрирующий работу класса Ship и содержащий игру "Морской бой"
 *
 * @author KSO 17ИТ17
 */
public class DemoSeaBattle {
    public static void main(String[] args) {
        Ship ship = new Ship((int) (Math.random() * 7));

        seaBattle(ship);
    }

    /**
     * Игра морской бой
     *
     * @param ship корабль
     */
    private static void seaBattle(Ship ship) {
        ArrayList<Integer> shots = new ArrayList<>();
        int attempts = 0;
        while (!ship.isDestroyed()) {
            System.out.print("Введите координату от 0 до 9: ");
            int shot = MyScanner.scanner.nextInt();
            attempts++;
            while (shot > 9 || shot < 0) {
                System.out.print("Вы вышли за границу, введите координату от 0 до 9: ");
                shot = MyScanner.scanner.nextInt();
                attempts++;
            }
            if (Ship.isRepeatedShot(shots, shot)) {
                System.out.println("Вы сюда уже стреляли!!!");
            } else if (ship.isHit(shot)) {
                if (ship.isDestroyed()) {
                    System.out.println("Вы убили корабль за " + attempts + " попыток");
                } else {
                    System.out.println("Попал");
                }
            } else {
                System.out.println("Мимо");
            }
            shots.add(shot);
        }

    }


}