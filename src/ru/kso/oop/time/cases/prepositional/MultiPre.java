package ru.kso.oop.time.cases.prepositional;

import ru.kso.oop.time.cases.ICase;
/**
 * Класс, содержащий метод для возвращения окончания для слова
 * 1-2 склонений, множественного числа, предложного падежа
 *
 * @author KSO 17ИТ17
 */
public class MultiPre implements ICase {
    @Override
    public String caseOfWord() {
        return "ах";
    }
}
