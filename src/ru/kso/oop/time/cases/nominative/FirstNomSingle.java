package ru.kso.oop.time.cases.nominative;

import ru.kso.oop.time.cases.ICase;

/**
 * Класс, содержащий метод для возвращения окончания для слова
 * первого склонения, единственного числа, именительного падежа
 *
 * @author KSO 17ИТ17
 */
public class FirstNomSingle implements ICase {

    @Override
    public String caseOfWord() {
        return "а";
    }
}
