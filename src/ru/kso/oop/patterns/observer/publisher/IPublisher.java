package ru.kso.oop.patterns.observer.publisher;

import java.util.ArrayList;

/**
 * Интерфейс, содержащий методы
 */
public interface IPublisher {
    /**
     * Метод для получения списка слушателей
     *
     * @return список слушателей
     */
    ArrayList<PublisherActionListener> getListeners();

    /**
     * Метод для добавления слушателя
     *
     * @param listener слушатель
     */
    void addListener(PublisherActionListener listener);

    /**
     * Метод для удаления слушателя
     *
     * @param listener слушатель
     */
    void removeListener(PublisherActionListener listener);

    /**
     * Метод для удаления всех слушателей
     */
    void removeAllListeners();

    /**
     * Метод для оповещения слушателей
     *
     * @param message сообщение
     */
    void notifySubscribers(String message);
}


