package ru.kso.fibonacci;

import ru.kso.MyScanner;

/**
 * Класс, содержащий метод рекурсивного вычисления числа Фибоначчи и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class NumberOfFibonacciRecursive {
    public static void main(String[] args) {
        System.out.print("Введите индекс числа Фибоначчи: ");
        int indexOfNumber = MyScanner.scanner.nextInt();
        System.out.println(fibonacciNumber(indexOfNumber));
    }

    /**
     * Рекурсивный метод, возвращающий число фибоначчи по индексу
     *
     * @param indexOfNumber индекс числа Фибоначчи
     * @return число Фибоначчи
     */
    private static int fibonacciNumber(int indexOfNumber) {
        if (indexOfNumber == 0 || indexOfNumber == 1) {
            return 1;
        }
        return fibonacciNumber(indexOfNumber - 1) + fibonacciNumber(indexOfNumber - 2);
    }
}
