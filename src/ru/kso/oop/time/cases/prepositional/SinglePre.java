package ru.kso.oop.time.cases.prepositional;

import ru.kso.oop.time.cases.ICase;
/**
 * Класс, содержащий метод для возвращения окончания для слова
 * 1-2 склонений, единственного числа, предложного падежа
 *
 * @author KSO 17ИТ17
 */
public class SinglePre implements ICase {
    @Override
    public String caseOfWord() {
        return "е";
    }
}
