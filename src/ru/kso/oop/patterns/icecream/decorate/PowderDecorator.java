package ru.kso.oop.patterns.icecream.decorate;

import ru.kso.oop.patterns.icecream.product.IceCream;

/**
 * Класс, расширяющий класс Decorator
 *
 * @author KSO 17ИТ17
 */
public class PowderDecorator extends Decorator {

    public PowderDecorator(IceCream iceCream) {
        super(iceCream);
    }

    /**
     * Метод для добавления присыпки
     */
    @Override
    void afterCook() {
        System.out.println("Присыпка добавлена");
    }
}
