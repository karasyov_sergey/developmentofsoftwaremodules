package ru.kso.withoutRepeat;

import java.util.ArrayList;

/**
 * Класс для перестройки массивов в массивы с не повторяющимися элементами
 *
 * @author KSO 17ИТ17
 */
public class MyMethod {
    public static void main(String[] args) {
        ArrayList<Object> numbersOne = new ArrayList<>();
        ArrayList<Object> strings = new ArrayList<>();
        strings.add("Текст 1");
        strings.add("Текст 2");
        strings.add("Текст 1");
        fillByIntegerNumbers(numbersOne);
        System.out.println(numbersOne);
        System.out.println(strings);
        noRepeat(numbersOne);
        noRepeat(strings);
        System.out.println(numbersOne);
        System.out.println(strings);
    }

    /**
     * Метод для перестройки массива{@code arrayList} в массив с неповторяющимися элементами
     *
     * @param arrayList списочный массив
     */
    private static void noRepeat(ArrayList<Object> arrayList) {
        ArrayList<Object> temp = new ArrayList<>(arrayList);
        arrayList.removeAll(temp);
        while (!temp.isEmpty()) {
            arrayList.add(temp.get(0));
            temp.removeAll(arrayList);
        }
    }

    /**
     * Метод для заполнения списочного массива {@code numbers} рандомными числами
     *
     * @param numbers пустой списочный массив
     */
    private static void fillByIntegerNumbers(ArrayList<Object> numbers) {
        int size = (int) (5 + Math.random() * 10);
        for (int index = 0; index < size; index++) {
            numbers.add((int) (Math.random() * 10));
        }
    }
}
