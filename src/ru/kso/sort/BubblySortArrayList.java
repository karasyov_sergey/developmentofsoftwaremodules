package ru.kso.sort;

import java.util.ArrayList;

/**
 * Класс, содержащий метод сортировки списочного массива {@code numbers} и его реализацию
 *
 * @author KSO 17ИТ17
 */
public class BubblySortArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        fill(numbers);
        inverseBubblySort(numbers);
        System.out.println(numbers);
    }

    /**
     * Метод, задающий рандомный размер списочного массива {@code arrayList} и заполняющий от 0 до n с шагом в 1
     *
     * @param arrayList списочный массив
     */
    private static void fill(ArrayList<Integer> arrayList) {
        for (int index = 0; index < ((int) (5 + Math.random() * 10)); index++) {
            arrayList.add(index);
        }
    }

    /**
     * Метод, сортирующий списочный массив {@code arrayList} по убыванию
     *
     * @param arrayList списочный массив, содержащий целые числа
     */
    private static void inverseBubblySort(ArrayList<Integer> arrayList) {
        for (int out = arrayList.size() - 1; out > 0; out--) {
            for (int index = 0; index < out; index++) {
                if (arrayList.get(index) < arrayList.get(index + 1)) {
                    int temp = arrayList.get(index);
                    arrayList.set(index, arrayList.get(index + 1));
                    arrayList.set(index + 1, temp);

                }
            }
        }
    }
}
