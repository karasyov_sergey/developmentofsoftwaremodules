package ru.kso.oop.patterns.state.context;


import ru.kso.oop.patterns.state.state.*;

/**
 * Класс, инициализирующий объект для смены состояния объекта
 *
 * @author KSO 17ИТ17
 */
public class TransformerContext implements TransformerState {
    private TransformerState state;

    public TransformerState getState() {
        return state;
    }

    /**
     * Изменения состояния
     *
     * @param newState новое состояние
     */
    public void setState(States newState) {
        switch (newState) {
            case FLY:
                this.state = new FlyState();
                break;
            case FIRE:
                this.state = new FireState();
                break;
            case MOVE:
                this.state = new MoveState();
                break;
            case COMBO:
                this.state = new ComboState();
        }

    }

    /**
     * Действие выполняемое обькт-состоянием
     */
    @Override
    public void action() {
        state.action();
    }
}
