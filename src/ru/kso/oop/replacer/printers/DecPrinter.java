package ru.kso.oop.replacer.printers;

/**
 * Класс, имплеминтирующий интерфейс iPrinter
 *
 * @author KSO 17ИТ17
 */
public class DecPrinter implements IPrinter {
    /**
     * Декоративный принтер
     *
     * @param text текст
     */
    @Override
    public void print(String text) {
        for (int index = 0; index < text.length(); index++) {
            System.out.print("*");
        }
        System.out.println("\n" + text);
        for (int index = 0; index < text.length(); index++) {
            System.out.print("*");
        }
    }
}
