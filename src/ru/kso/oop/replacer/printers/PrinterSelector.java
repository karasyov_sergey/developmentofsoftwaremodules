package ru.kso.oop.replacer.printers;

/**
 * Класс для выбора принтера
 *
 * @author KSO 17ИТ17
 */
public class PrinterSelector {
    /**
     * Метод, организующий выбор принтера
     *
     * @param type тип принтера
     * @return принтер
     */
    public IPrinter getPrinter(TypeOfPrinters type) {
        IPrinter printer = null;
        switch (type){
            case DEC_PRINTER:
                printer = new DecPrinter();
                break;
            case CONSOLE_PRINTER:
                printer = new ConsolePrinter();
                break;
            case ADV_CONSOLE_PRINTER:
                printer = new AdvConsolePrinter();
                break;
        }
        return printer;
    }
}
