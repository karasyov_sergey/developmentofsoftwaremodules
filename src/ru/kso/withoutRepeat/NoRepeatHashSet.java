package ru.kso.withoutRepeat;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Класс демонстрирующий работу HashSet
 *
 * @author KSO 17ИТ17
 */
public class NoRepeatHashSet {
    public static void main(String[] args) {
        ArrayList<Integer> numbersOne = new ArrayList<>();
        ArrayList<String> stringsOne = new ArrayList<>();
        stringsOne.add("Текст 1");
        stringsOne.add("Текст 2");
        stringsOne.add("Текст 1");
        fill(numbersOne);
        System.out.println(numbersOne);
        System.out.println(stringsOne);
        HashSet<Integer> numbersTwo = new HashSet<>(numbersOne);
        HashSet<String> stringsTwo = new HashSet<>(stringsOne);
        System.out.println(numbersTwo);
        System.out.println(stringsTwo);
    }

    /**
     * Метод для заполнения списочного массива {@code numbers} рандомными числами
     *
     * @param numbers пустой списочный массив
     */
    private static void fill(ArrayList<Integer> numbers) {
        int size = (int) (5 + Math.random() * 10);
        for (int index = 0; index < size; index++) {
            numbers.add((int) (Math.random() * 10));
        }
    }
}
