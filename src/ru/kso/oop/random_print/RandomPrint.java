package ru.kso.oop.random_print;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс для чтения фамилий студентов с файла, запись на лист и рандомного вывода на экран
 *
 * @author KSO 17ИТ17
 */
public class RandomPrint {
    public static void main(String[] args) throws IOException {
        List<String> list = Files.readAllLines(Paths.get("src/ru/kso/oop/random_print/list.txt"));
        randomPrint(list);
    }

    /**
     * Метод для рандомного вывода на экран фамилий студентов
     *
     * @param list лист, содержащий фамилии студентов
     */
    private static void randomPrint(List<String> list) {
        while (!list.isEmpty()) {
            int index = (int) (Math.random() * list.size());
            System.out.println(list.get(index));
            list.remove(index);
        }
    }
}
