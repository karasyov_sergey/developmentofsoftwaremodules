package ru.kso.oop.regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, предназначенный для поиска числа с плавающей точкой
 *
 * @author KSO 17ИТ17
 */
public class DoubleReg {
    public static void main(String[] args) {
        String string = "2.5 делить на -0.3e+11 -7.33 плюс 17.33e-11 умножить на 3.2e11 " ;
        Pattern pattern =
                Pattern.compile("([+-]?\\d+\\.\\d*[e][+-]*\\d+)");

        Matcher matcher = pattern.matcher(string);
        while (matcher.find()){
            System.out.println(matcher.group());
        }
    }
}
